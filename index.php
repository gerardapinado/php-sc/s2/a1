<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s2: Activity</title>
</head>
<body>
    <?php 
        printDivisibleBy5();
        echo "<div style=\"border: 1px solid black; width: 25%; padding: 20px\">";
        $students = array();
        array_push($students, "John Smith");
        print_r($students);
        echo "<br/>";
        echo count($students);
        echo "<br/>";
        array_push($students, "Jane Smith");
        print_r($students);
        echo "<br/>";
        echo count($students);
        echo "<br/>";
        array_shift($students);
        print_r($students);
        echo "<br/>";
        echo count($students);
        echo "</div>";
    ?>
</body>
</html>